/*
 * Copyright (C) 2018 Federico Iosue (federico.iosue@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package it.feio.android.omninotes;


import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Build;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.AppCompatImageView;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CategoryLifecycleTest extends BaseEspressoTest {

    private String categoryName;

    @Test
    public void addNewCategory() {

        categoryName = "Cat_" + Calendar.getInstance().getTimeInMillis();

        ViewInteraction viewInteraction = onView(
                allOf(withId(R.id.fab_expand_menu_button),
                        withParent(withId(R.id.fab)),
                        isDisplayed()));
        viewInteraction.perform(click());

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.fab_note),
                        withParent(withId(R.id.fab)),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.menu_category), withContentDescription(R.string.category), isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction mDButton = onView(
                allOf(withId(R.id.buttonDefaultPositive), withText(R.string.add_category), isDisplayed()));
        mDButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.category_title), isDisplayed()));
        appCompatEditText.perform(replaceText(categoryName), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.save), withText("Ok"), isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction editText = onView(
                allOf(withId(R.id.detail_title),
                        withParent(allOf(withId(R.id.title_wrapper),
                                withParent(withId(R.id.detail_tile_card)))),
                        isDisplayed()));
        editText.perform(click());

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.detail_title),
                        withParent(allOf(withId(R.id.title_wrapper),
                                withParent(withId(R.id.detail_tile_card)))),
                        isDisplayed()));
        editText2.perform(replaceText("Note with new category"), closeSoftKeyboard());

        ViewInteraction navigationUp = onView(
                allOf(withContentDescription(R.string.drawer_open),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        navigationUp.perform(click());

    }

    @Test
    public void checkCategoryCreation() {

        addNewCategory();

        ViewInteraction drawerToggle = onView(
                allOf(withContentDescription(R.string.drawer_open),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        drawerToggle.perform(click());

        ViewInteraction textView = onView(allOf(withId(R.id.title), withText(categoryName)));
        textView.check(matches(withText(categoryName)));
    }

    @Test
    public void categoryColorChange() {

        addNewCategory();

        ViewInteraction drawerToggle = onView(
                allOf(withContentDescription(R.string.drawer_open),
                        withParent(withId(R.id.toolbar))));
        drawerToggle.perform(click());

        ViewInteraction categoryView = onView(allOf(withId(R.id.title), withText(categoryName)));
        categoryView.perform(longClick());

        ViewInteraction imageView = onView(allOf(withId(R.id.color_chooser), isDisplayed()));
        imageView.check(matches(isDisplayed()));

        ViewInteraction appCompatImageView = onView(allOf(withId(R.id.color_chooser), isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction mDButton2 = onView(
                allOf(withId(R.id.buttonDefaultNeutral), withText("Custom"),
                        withParent(allOf(withId(R.id.root),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        mDButton2.perform(click());

        ViewInteraction mDButton3 = onView(
                allOf(withId(R.id.buttonDefaultNeutral), withText(R.string.md_presets_label),
                        withParent(allOf(withId(R.id.root),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        mDButton3.perform(click());

        ViewInteraction circleView = onView(
                childAtPosition(
                        withId(R.id.grid),
                        18));
        circleView.perform(scrollTo(), click());

        ViewInteraction circleView2 = onView(
                childAtPosition(
                        withId(R.id.grid),
                        9));
        circleView2.perform(scrollTo(), click());

        ViewInteraction mDButton4 = onView(
                allOf(withId(R.id.buttonDefaultPositive), withText(R.string.md_done_label),
                        withParent(allOf(withId(R.id.root),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        mDButton4.perform(click());

        ViewInteraction appCompatImageViewColorChanged = onView(allOf(withId(R.id.color_chooser), isDisplayed()));
        appCompatImageViewColorChanged.check(matches(withBackgroundColor(Color.parseColor("#FF263238"))));

    }

    @Test
    public void categoryDeletion() {

        addNewCategory();

        ViewInteraction drawerToggle = onView(
                allOf(withContentDescription(R.string.drawer_open),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        drawerToggle.perform(click());

        ViewInteraction categoryView = onView(allOf(withId(R.id.title), withText(categoryName)));
        categoryView.perform(longClick());

        ViewInteraction deleteBtn = onView(
                allOf(withId(R.id.delete), withText(R.string.delete), isDisplayed()));
        deleteBtn.perform(click());

        ViewInteraction deleteConfirmBtn = onView(
                allOf(withId(R.id.buttonDefaultPositive), withText(R.string.confirm), isDisplayed()));
        deleteConfirmBtn.perform(click());

        // Waiting a little to ensure Eventbus post propagation
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction categoryDeletedView = onView(allOf(withId(R.id.title), withText(categoryName)));
        categoryDeletedView.check(doesNotExist());

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Matcher<View> withBackgroundColor(final int backgroundColor) {
        return new TypeSafeMatcher<View>() {

            @Override
            public boolean matchesSafely(View view) {
                ColorFilter cf = new PorterDuffColorFilter(Color.parseColor("#FF263238"), PorterDuff.Mode.SRC_ATOP);
                ColorFilter cf1 = ((AppCompatImageView) view).getDrawable().getColorFilter();
                return cf.equals(cf1);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with background color from id: " + backgroundColor);
            }
        };
    }

    @LargeTest
    @RunWith(AndroidJUnit4.class)
    public static class SortFunctionTest {

        @Rule
        public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

        @Test
        public void sortFunctionTest() {
            ViewInteraction viewInteraction = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_expand_menu_button), ViewMatchers.isDisplayed()));
            viewInteraction.perform(ViewActions.click());



            ViewInteraction floatingActionButton = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_checklist), ViewMatchers.isDisplayed()));
            floatingActionButton.perform(ViewActions.click());

            ViewInteraction editText = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.detail_title), ViewMatchers.isDisplayed()));
            editText.perform(ViewActions.replaceText("1"), ViewActions.closeSoftKeyboard());

            ViewInteraction imageButton = Espresso.onView(
                    allOf(ViewMatchers.withContentDescription("drawer open"), ViewMatchers.isDisplayed()));
            imageButton.perform(ViewActions.click());

            SystemClock.sleep(5000);

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction viewInteraction2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_expand_menu_button), ViewMatchers.isDisplayed()));
                    viewInteraction2.perform(ViewActions.click());

            SystemClock.sleep(10000);

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction floatingActionButton2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_checklist), ViewMatchers.isDisplayed()));
            floatingActionButton2.perform(ViewActions.click());

            SystemClock.sleep(20000);

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction editText2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.detail_title), ViewMatchers.isDisplayed()));
            editText2.perform(ViewActions.replaceText("2"), ViewActions.closeSoftKeyboard());

            ViewInteraction imageButton2 = Espresso.onView(
                    allOf(ViewMatchers.withContentDescription("drawer open"), ViewMatchers.isDisplayed()));
            imageButton2.perform(ViewActions.click());

            SystemClock.sleep(5000);

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Espresso.openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().getTargetContext());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(40000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction appCompatTextView = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.title), ViewMatchers.withText("Sort"), ViewMatchers.isDisplayed()));
            appCompatTextView.perform(ViewActions.click());

            SystemClock.sleep(20000);

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction appCompatTextView2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.title), ViewMatchers.withText("Creation date"), ViewMatchers.isDisplayed()));
            appCompatTextView2.perform(ViewActions.click());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

           /* ViewInteraction textView = onView(
                    allOf(withId(R.id.note_title), withText("2"), isDisplayed()));
            textView.check(matches(withText("2")));

            ViewInteraction textView2 = onView(
                    allOf(withId(R.id.note_title), withText("1"), isDisplayed()));
            textView2.check(matches(withText("1")));*/

        }

        private static Matcher<View> childAtPosition(
                final Matcher<View> parentMatcher, final int position) {

            return new TypeSafeMatcher<View>() {
                @Override
                public void describeTo(Description description) {
                    description.appendText("Child at position " + position + " in parent ");
                    parentMatcher.describeTo(description);
                }

                @Override
                public boolean matchesSafely(View view) {
                    ViewParent parent = view.getParent();
                    return parent instanceof ViewGroup && parentMatcher.matches(parent)
                            && view.equals(((ViewGroup) parent).getChildAt(position));
                }
            };
        }
    }

    @LargeTest
    @RunWith(AndroidJUnit4.class)
    public static class SettingsTest {

        @Rule
        public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

        @Test
        public void settingsTest() {
            ViewInteraction imageButton = Espresso.onView(
                    allOf(ViewMatchers.withContentDescription("drawer open"), ViewMatchers.isDisplayed()));
            imageButton.perform(ViewActions.click());

            ViewInteraction linearLayout = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.settings_view),
                            childAtPosition(
                                    allOf(ViewMatchers.withId(R.id.left_drawer),
                                            childAtPosition(
                                                    ViewMatchers.withId(R.id.navigation_drawer),
                                                    0)),
                                    2)));
            linearLayout.perform(ViewActions.scrollTo(), ViewActions.click());

            DataInteraction linearLayout2 = Espresso.onData(anything())
                    .inAdapterView(allOf(ViewMatchers.withId(android.R.id.list),
                            childAtPosition(
                                    ViewMatchers.withClassName(is("android.widget.FrameLayout")),
                                    0)))
                    .atPosition(15);
            linearLayout2.perform(ViewActions.click());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction imageButton2 = Espresso.onView(
                    allOf(ViewMatchers.withContentDescription("Navigate up"), ViewMatchers.isDisplayed()));
            imageButton2.perform(ViewActions.click());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            DataInteraction linearLayout3 = Espresso.onData(anything())
                    .inAdapterView(allOf(ViewMatchers.withId(android.R.id.list),
                            childAtPosition(
                                    ViewMatchers.withClassName(is("android.widget.FrameLayout")),
                                    0)))
                    .atPosition(5);
            linearLayout3.perform(ViewActions.click());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            DataInteraction linearLayout4 = Espresso.onData(anything())
                    .inAdapterView(allOf(ViewMatchers.withId(android.R.id.list),
                            childAtPosition(
                                    ViewMatchers.withClassName(is("android.widget.FrameLayout")),
                                    0)))
                    .atPosition(0);
            linearLayout4.perform(ViewActions.click());

            ViewInteraction imageButton3 = Espresso.onView(
                    allOf(ViewMatchers.withContentDescription("Navigate up"), ViewMatchers.isDisplayed()));
            imageButton3.perform(ViewActions.click());

            DataInteraction linearLayout5 = Espresso.onData(anything())
                    .inAdapterView(allOf(ViewMatchers.withId(android.R.id.list),
                            childAtPosition(
                                    ViewMatchers.withClassName(is("android.widget.FrameLayout")),
                                    0)))
                    .atPosition(1);
            linearLayout5.perform(ViewActions.click());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(40000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            DataInteraction linearLayout6 = Espresso.onData(anything())
                    .inAdapterView(allOf(ViewMatchers.withId(android.R.id.list),
                            childAtPosition(
                                    ViewMatchers.withClassName(is("android.widget.FrameLayout")),
                                    0)))
                    .atPosition(3);
            linearLayout6.perform(ViewActions.click());

           /* ViewInteraction mDButton = onView(
                    allOf(withId(R.id.buttonDefaultPositive), withText("Confirm"),
                            childAtPosition(
                                    childAtPosition(
                                            withId(android.R.id.content),
                                            0),
                                    4),
                            isDisplayed()));
            mDButton.perform(click());*/

        }

        private static Matcher<View> childAtPosition(
                final Matcher<View> parentMatcher, final int position) {

            return new TypeSafeMatcher<View>() {
                @Override
                public void describeTo(Description description) {
                    description.appendText("Child at position " + position + " in parent ");
                    parentMatcher.describeTo(description);
                }

                @Override
                public boolean matchesSafely(View view) {
                    ViewParent parent = view.getParent();
                    return parent instanceof ViewGroup && parentMatcher.matches(parent)
                            && view.equals(((ViewGroup) parent).getChildAt(position));
                }
            };
        }
    }

    @LargeTest
    @RunWith(AndroidJUnit4.class)
    public static class SearchFunctionTest {

        @Rule
        public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

        @Test
        public void searchFunctionTest() {
            ViewInteraction viewInteraction = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_expand_menu_button), ViewMatchers.isDisplayed()));
            viewInteraction.perform(ViewActions.click());

            ViewInteraction floatingActionButton = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_checklist), ViewMatchers.isDisplayed()));
            floatingActionButton.perform(ViewActions.click());

            ViewInteraction editText = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.detail_title), ViewMatchers.isDisplayed()));
            editText.perform(ViewActions.replaceText("text"), ViewActions.closeSoftKeyboard());

            ViewInteraction imageButton = Espresso.onView(
                    allOf(ViewMatchers.withContentDescription("drawer open"), ViewMatchers.isDisplayed()));
            imageButton.perform(ViewActions.click());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction viewInteraction2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_expand_menu_button), ViewMatchers.isDisplayed()));
            viewInteraction2.perform(ViewActions.click());

            ViewInteraction floatingActionButton2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_checklist), ViewMatchers.isDisplayed()));
            floatingActionButton2.perform(ViewActions.click());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction editText2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.detail_title), ViewMatchers.isDisplayed()));
            editText2.perform(ViewActions.replaceText("1"), ViewActions.closeSoftKeyboard());

            ViewInteraction imageButton2 = Espresso.onView(
                    allOf(ViewMatchers.withContentDescription("drawer open"), ViewMatchers.isDisplayed()));
            imageButton2.perform(ViewActions.click());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(40000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction actionMenuItemView = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.menu_search), ViewMatchers.withContentDescription("Search"), ViewMatchers.isDisplayed()));
            actionMenuItemView.perform(ViewActions.click());


            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction searchAutoComplete = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.search_src_text), ViewMatchers.isDisplayed()));
            searchAutoComplete.perform(ViewActions.replaceText("te"), ViewActions.closeSoftKeyboard());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction searchAutoComplete2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.search_src_text), ViewMatchers.withText("te"), ViewMatchers.isDisplayed()));
            searchAutoComplete2.perform(ViewActions.replaceText("text"));

            ViewInteraction searchAutoComplete3 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.search_src_text), ViewMatchers.withText("text"), ViewMatchers.isDisplayed()));
            searchAutoComplete3.perform(ViewActions.closeSoftKeyboard());

            ViewInteraction searchAutoComplete4 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.search_src_text), ViewMatchers.withText("text"), ViewMatchers.isDisplayed()));
            searchAutoComplete4.perform(ViewActions.pressImeActionButton());

        }

        private static Matcher<View> childAtPosition(
                final Matcher<View> parentMatcher, final int position) {

            return new TypeSafeMatcher<View>() {
                @Override
                public void describeTo(Description description) {
                    description.appendText("Child at position " + position + " in parent ");
                    parentMatcher.describeTo(description);
                }

                @Override
                public boolean matchesSafely(View view) {
                    ViewParent parent = view.getParent();
                    return parent instanceof ViewGroup && parentMatcher.matches(parent)
                            && view.equals(((ViewGroup) parent).getChildAt(position));
                }
            };
        }
    }

    @LargeTest
    @RunWith(AndroidJUnit4.class)
    public static class ReminderTest {

        @Rule
        public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

        @Test
        public void reminderTest() {
            ViewInteraction viewInteraction = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_expand_menu_button), ViewMatchers.isDisplayed()));
            viewInteraction.perform(ViewActions.click());

            ViewInteraction floatingActionButton = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_note), ViewMatchers.isDisplayed()));
            floatingActionButton.perform(ViewActions.click());

            ViewInteraction editText = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.detail_title), ViewMatchers.isDisplayed()));
            editText.perform(ViewActions.replaceText("1"), ViewActions.closeSoftKeyboard());

            ViewInteraction linearLayout = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.reminder_layout),
                            childAtPosition(
                                    childAtPosition(
                                            ViewMatchers.withClassName(is("android.widget.LinearLayout")),
                                            1),
                                    2)));
            linearLayout.perform(ViewActions.scrollTo(), ViewActions.click());

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(40);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction appCompatButton = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.done), ViewMatchers.withText("Done"), ViewMatchers.isDisplayed()));
            appCompatButton.perform(ViewActions.click());

            ViewInteraction appCompatButton2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.done_button), ViewMatchers.withText("Done"), ViewMatchers.isDisplayed()));
            appCompatButton2.perform(ViewActions.click());

            ViewInteraction appCompatButton3 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.done), ViewMatchers.withText("Done"), ViewMatchers.isDisplayed()));
            appCompatButton3.perform(ViewActions.click());

        }

        private static Matcher<View> childAtPosition(
                final Matcher<View> parentMatcher, final int position) {

            return new TypeSafeMatcher<View>() {
                @Override
                public void describeTo(Description description) {
                    description.appendText("Child at position " + position + " in parent ");
                    parentMatcher.describeTo(description);
                }

                @Override
                public boolean matchesSafely(View view) {
                    ViewParent parent = view.getParent();
                    return parent instanceof ViewGroup && parentMatcher.matches(parent)
                            && view.equals(((ViewGroup) parent).getChildAt(position));
                }
            };
        }
    }

    @LargeTest
    @RunWith(AndroidJUnit4.class)
    public static class CreateChecklistTest {

        @Rule
        public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

        @Test
        public void createChecklistTest() {
            ViewInteraction viewInteraction = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_expand_menu_button), ViewMatchers.isDisplayed()));
            viewInteraction.perform(ViewActions.click());

            ViewInteraction floatingActionButton = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.fab_checklist), ViewMatchers.isDisplayed()));
            floatingActionButton.perform(ViewActions.click());

            ViewInteraction editText = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.detail_title), ViewMatchers.isDisplayed()));
            editText.perform(ViewActions.replaceText("check"), ViewActions.closeSoftKeyboard());

            ViewInteraction editText2 = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.detail_title), ViewMatchers.withText("check"), ViewMatchers.isDisplayed()));
            editText2.perform(ViewActions.pressImeActionButton());

            ViewInteraction imageButton = Espresso.onView(
                    allOf(ViewMatchers.withContentDescription("drawer open"), ViewMatchers.isDisplayed()));
            imageButton.perform(ViewActions.click());

            SystemClock.sleep(10000);

            // Added a sleep statement to match the app's execution delay.
            // The recommended way to handle such scenarios is to use Espresso idling resources:
            // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ViewInteraction textView = Espresso.onView(
                    allOf(ViewMatchers.withId(R.id.note_title), ViewMatchers.withText("check"), ViewMatchers.isDisplayed()));
            textView.check(ViewAssertions.matches(ViewMatchers.withText("check")));

            SystemClock.sleep(20000);
        }


        private static Matcher<View> childAtPosition(
                final Matcher<View> parentMatcher, final int position) {

            return new TypeSafeMatcher<View>() {
                @Override
                public void describeTo(Description description) {
                    description.appendText("Child at position " + position + " in parent ");
                    parentMatcher.describeTo(description);
                }

                @Override
                public boolean matchesSafely(View view) {
                    ViewParent parent = view.getParent();
                    return parent instanceof ViewGroup && parentMatcher.matches(parent)
                            && view.equals(((ViewGroup) parent).getChildAt(position));
                }
            };
        }
    }
}
